//------------------------------------------------------------------------------
/**
* wxform.cpp
* - implementation for main wx-based form
**/
//------------------------------------------------------------------------------
#include "wxform.hpp"
#include <wx/aboutdlg.h>
//------------------------------------------------------------------------------
#include "../res/apps.xpm"
#include "../res/exit.xpm"
#include "../res/quest.xpm"
//------------------------------------------------------------------------------
#define MACRO_WXBMP(bmp) wxBitmap(bmp##_xpm)
#define MACRO_WXICO(bmp) wxIcon(bmp##_xpm)
//------------------------------------------------------------------------------
#define MESSAGE_WELCOME "Welcome to MY1TERMW\n"
#define DEFAULT_PROMPT "my1term> "
#define CONS_WIDTH 640
#define CONS_HEIGHT 480
//------------------------------------------------------------------------------
void TestPrompt(void* from) {
	my1Term* that;
	wxString ccmd;
	that = (my1Term*) from;
	ccmd = that->GetCommand();
	printf("@@ Command:%s\n",ccmd.mb_str().data());
}
//------------------------------------------------------------------------------
my1Form::my1Form(const wxString &title)
		: wxFrame( NULL, MY1ID_MAIN, title, wxDefaultPosition,
		wxDefaultSize, wxDEFAULT_FRAME_STYLE) {
	// initialize stuffs
	mTerm = new my1Term(this,wxID_ANY);
	// setup image
	wxIcon mIconApps = MACRO_WXICO(apps);
	this->SetIcon(mIconApps);
	// create tool bar
	//wxToolBar* mainTool = this->CreateToolBar(wxBORDER_NONE|wxTB_HORIZONTAL|
	//	wxTB_FLAT,MY1ID_MAIN_TOOL,wxT("mainTool"));
	//mainTool->SetToolBitmapSize(wxSize(16,16));
	wxToolBar* mainTool = mTerm->Toolbar();
	mainTool->AddSeparator();
	wxBitmap mIconAbout = MACRO_WXBMP(quest);
	mainTool->AddTool(MY1ID_ABOUT, wxT(""), mIconAbout,wxT("About my1termw"));
	mainTool->AddSeparator();
	wxBitmap mIconExit = MACRO_WXBMP(exit);
	mainTool->AddTool(MY1ID_EXIT,wxT(""),mIconExit,wxT("Exit my1termw"));
	mainTool->Realize();
	// duh!
	this->SetClientSize(wxSize(CONS_WIDTH,CONS_HEIGHT));
	// actions & events
	mTerm->Connect(MY1ID_EXIT,wxEVT_COMMAND_TOOL_CLICKED,
		wxCommandEventHandler(my1Form::OnExit));
	mTerm->Connect(MY1ID_ABOUT,wxEVT_COMMAND_TOOL_CLICKED,
		wxCommandEventHandler(my1Form::OnAbout));
	// write welcome message
	mTerm->WriteConsole(MESSAGE_WELCOME);
	mTerm->SetPrompt(DEFAULT_PROMPT);
	mTerm->OnCommand = TestPrompt;
	// position this!
	this->Centre();
}
//------------------------------------------------------------------------------
my1Form::~my1Form() {
	// nothing to do
}
//------------------------------------------------------------------------------
void my1Form::OnExit(wxCommandEvent& event) {
	my1Form* form = (my1Form*)this->GetParent();
	form->Close();
}
//------------------------------------------------------------------------------
void my1Form::OnAbout(wxCommandEvent& event) {
	wxAboutDialogInfo cAboutInfo;
	wxString cDescription = wxString::Format("%s",
		"\nGUI for my1termu (Serial Port Interface)!\n");
	cAboutInfo.SetName(MY1APP_PROGNAME);
	cAboutInfo.SetVersion(MY1APP_PROGVERS);
	cAboutInfo.SetDescription(cDescription);
	cAboutInfo.SetCopyright("Copyright (C) " MY1APP_LICENSE_);
	cAboutInfo.SetWebSite("http://www.my1matrix.org");
	cAboutInfo.AddDeveloper("Azman M. Yusof <azman@my1matrix.org>");
	wxAboutBox(cAboutInfo,this);
}
//------------------------------------------------------------------------------
