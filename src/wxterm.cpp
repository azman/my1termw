//------------------------------------------------------------------------------
/**
* wxterm.cpp
* - implementation for wx-based terminal interface panel
**/
//------------------------------------------------------------------------------
#include "wxterm.hpp"
#include <wx/stdpaths.h>
#include <wx/fileconf.h>
#include <wx/wfstream.h>
//------------------------------------------------------------------------------
#include "../res/connect.xpm"
#include "../res/disconnect.xpm"
#include "../res/target.xpm"
#include "../res/ichange.xpm"
#include "../res/newempty.xpm"
#include "../res/newticked.xpm"
#include "../res/option.xpm"
#include "../res/linectrl.xpm"
//------------------------------------------------------------------------------
#ifndef MACRO_WXBMP
#define MACRO_WXBMP(bmp) wxBitmap(bmp##_xpm)
#define MACRO_WXICO(bmp) wxIcon(bmp##_xpm)
#endif
//------------------------------------------------------------------------------
#define CONS_FONT_SIZE 8
//------------------------------------------------------------------------------
wxIMPLEMENT_DYNAMIC_CLASS(my1Term, wxWindow);
wxDECLARE_EVENT(wxEVT_COMMAND_MYTHREAD_UPDATE, wxThreadEvent);
wxDEFINE_EVENT(wxEVT_COMMAND_MYTHREAD_UPDATE, wxThreadEvent);
//------------------------------------------------------------------------------
void AddMenuImage(wxMenu *aMenu, wxWindowID anID,
		const char* aLabel, wxBitmap& aBitmap) {
	wxMenuItem* pItem = aMenu->Append(anID, wxString(aLabel));
	aMenu->Remove(pItem);
	pItem->SetBitmap(aBitmap);
	aMenu->Append(pItem);
}
//------------------------------------------------------------------------------
my1Term::my1Term(wxWindow *parent,wxWindowID id,
		const wxPoint& point,const wxSize& size) :
		wxPanel(parent,id,point,size) {
	// initialize stuffs
	this->PortInit();
	mOptions.mSerialConf.mPortIndex = mPortControl.term;
	mOptions.mSerialConf.mDeviceName = mPortControl.name;
	mOptions.mSerialConf.mBaudRate = mPortConfig.baud;
	mOptions.mLocalEcho = false;
	mOptions.mShowHex = false;
	mOptions.mWin32CRLF = false;
	mThreadChk = 0x0;
	mThreadRunning = false;
	mCheckingData = false;
	mDoCommand = false;
	// initialize function pointer
	OnCommand = 0x0;
	// get program path/name
	wxStandardPaths& cPaths = wxStandardPaths::Get();
	mThisExec.Assign(cPaths.GetExecutablePath());
	this->ConfigPath(mThisExec.GetPath());
	// create main popup menu
	mMenuTerm = new wxMenu;
	// insert port select
	mMenuPort = new wxMenu;
	//this->GoPortMenu();
	mMenuTerm->Append(MY1ID_TERM_PORT, wxT("Port Select"), mMenuPort);
	// insert device name
	wxMenu* pMenuName = new wxMenu;
	pMenuName->Append(MY1ID_TERM_DEVN,mOptions.mSerialConf.mDeviceName);
	mMenuTerm->Append(MY1ID_TERM_NAME, wxT("Port Device"), pMenuName);
#ifdef DO_MINGW
	// not needed on win32? disable!
	wxMenuItem* pItemName = mMenuTerm->FindItem(MY1ID_TERM_DEVN);
	pItemName->Enable(false);
#endif
	// insert baud options
	char cBaudText[][8] = {"9600","19200","38400","57600","115200",
		"1200","2400","4800"};
	mMenuBaud = new wxMenu;
	wxMenu* pMenuBaud = new wxMenu;
	for (int cLoop=5;cLoop<8;cLoop++) {
		mMenuBaud->Append(MY1ID_TERM_RATE0+cLoop,cBaudText[cLoop],
			wxEmptyString,wxITEM_RADIO);
		pMenuBaud->Append(MY1ID_TERM_BAUD0+cLoop,cBaudText[cLoop],
			wxEmptyString,wxITEM_RADIO);
	}
	for (int cLoop=0;cLoop<5;cLoop++) {
		mMenuBaud->Append(MY1ID_TERM_RATE0+cLoop,cBaudText[cLoop],
			wxEmptyString,wxITEM_RADIO);
		pMenuBaud->Append(MY1ID_TERM_BAUD0+cLoop,cBaudText[cLoop],
			wxEmptyString,wxITEM_RADIO);
	}
	mMenuTerm->Append(MY1ID_TERM_BAUD, wxT("Port Baudrate"), pMenuBaud);
	// insert port options
	wxMenu* pMenuOpts = new wxMenu;
	pMenuOpts->Append(MY1ID_TERM_DOECHO,wxT("Local Echo"),
		wxEmptyString,wxITEM_CHECK);
	pMenuOpts->Append(MY1ID_TERM_SHOWHEX,wxT("Show HEX"),
		wxEmptyString,wxITEM_CHECK);
	pMenuOpts->Append(MY1ID_TERM_WIN32LF,wxT("Win32 CRLF"),
		wxEmptyString,wxITEM_CHECK);
	mMenuTerm->Append(wxID_ANY, wxT("Port Options"), pMenuOpts);
	mMenuOpts = new wxMenu;
	mMenuOpts->Append(MY1ID_TERM_DOECHO1,wxT("Local Echo"),
		wxEmptyString,wxITEM_CHECK);
	mMenuOpts->Append(MY1ID_TERM_SHOWHEX1,wxT("Show HEX"),
		wxEmptyString,wxITEM_CHECK);
	mMenuOpts->Append(MY1ID_TERM_WIN32LF1,wxT("Win32 CRLF"),
		wxEmptyString,wxITEM_CHECK);
	// line status menu
	wxMenu* pMenuLine = new wxMenu;
	pMenuLine->Append(MY1ID_TERM_DTR1,wxT("DTR"),
		wxEmptyString,wxITEM_CHECK);
	wxMenuItem* pItemDSR = pMenuLine->Append(MY1ID_TERM_DSR1,wxT("DSR"),
		wxEmptyString,wxITEM_CHECK);
	pItemDSR->Enable(false);
	pMenuLine->Append(MY1ID_TERM_RTS1,wxT("RTS"),
		wxEmptyString,wxITEM_CHECK);
	wxMenuItem* pItemCTS = pMenuLine->Append(MY1ID_TERM_CTS1,wxT("CTS"),
		wxEmptyString,wxITEM_CHECK);
	pItemCTS->Enable(false);
	mMenuTerm->Append(MY1ID_TERM_LINE, wxT("Port Lines"), pMenuLine);
	// main menu
	mMenuTerm->AppendSeparator();
	mIconInit = MACRO_WXBMP(connect);
	mIconStop = MACRO_WXBMP(disconnect);
	wxBitmap mIconFind = MACRO_WXBMP(target);
	wxBitmap mIconCopy = MACRO_WXBMP(ichange);
	wxBitmap mIconZero = MACRO_WXBMP(newempty);
	wxBitmap mIconSend = MACRO_WXBMP(newticked);
	wxBitmap mIconOpts = MACRO_WXBMP(option);
	wxBitmap mIconBaud = MACRO_WXBMP(linectrl);
	AddMenuImage(mMenuTerm,MY1ID_TERM_INIT,"Connect",mIconInit);
	AddMenuImage(mMenuTerm,MY1ID_TERM_STOP,"Disconnect",mIconStop);
	AddMenuImage(mMenuTerm,MY1ID_TERM_FIND,"Refresh",mIconFind);
	AddMenuImage(mMenuTerm,MY1ID_TERM_SEND,"Send File",mIconSend);
	mMenuTerm->AppendSeparator();
	AddMenuImage(mMenuTerm,MY1ID_TERM_COPY,"Copy",mIconCopy);
	AddMenuImage(mMenuTerm,MY1ID_TERM_ZERO,"Clear",mIconZero);
	mMenuTerm->AppendSeparator();
	mMenuTerm->Append(MY1ID_TERM_LOAD,wxT("Load Settings"));
	mMenuTerm->Append(MY1ID_TERM_SAVE,wxT("Save Settings"));
	// create console
	mConsole = new wxTextCtrl(this, MY1ID_TERM, wxT(""),
		wxDefaultPosition, wxDefaultSize,
		wxTE_MULTILINE|wxTE_RICH, wxDefaultValidator); //|wxTE_READONLY
	// create bar
	wxPanel* cBar1 = new wxPanel(this);
	mTool = new wxToolBar(cBar1,wxID_ANY,wxDefaultPosition,
		wxDefaultSize,wxTB_HORIZONTAL|wxTB_FLAT);
	// smaller toolbar by default?
	mTool->SetToolBitmapSize(FromDIP(wxSize(16,16)));
	wxFont temp = mTool->GetFont().Smaller();
	temp.Scale(1.1);
	mTool->SetFont(temp);
	mBoxT = new wxComboBox(mTool,wxID_ANY);
	mTool->AddControl(mBoxT,wxT("Port Select"));
	mTool->AddTool(MY1ID_TERM_FIND,"Refresh",mIconFind,"Refresh");
	mConn = mTool->AddTool(MY1ID_TERM_TOGL,"Connect",mIconInit,"Connect");
	mTool->AddTool(MY1ID_TERM_RATE,"Baudrate",mIconBaud,"Baudrate");
	mTool->AddTool(MY1ID_TERM_OPTS,">>",mIconOpts,">>");
	wxBoxSizer *pSize1 = new wxBoxSizer(wxHORIZONTAL);
	pSize1->Add(mTool,0,wxEXPAND);
	cBar1->SetSizer(pSize1);
	this->UpdatePortList();
	// main sizer
	wxBoxSizer *pSizerMain = new wxBoxSizer(wxVERTICAL);
	pSizerMain->Add(cBar1,0);
	pSizerMain->Add(mConsole,1,wxEXPAND);
	this->SetSizerAndFit(pSizerMain);
	// set custom font
	wxFont cFont(CONS_FONT_SIZE,wxFONTFAMILY_TELETYPE,
		wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,
		false,wxEmptyString,wxFONTENCODING_ISO8859_1);
	mConsole->SetFont(cFont);
	// key event for console
	mConsole->Connect(MY1ID_TERM,wxEVT_CHAR,
		wxKeyEventHandler(my1Term::OnKeyConsole),NULL,this);
	// popup menu event
	mConsole->Connect(MY1ID_TERM,wxEVT_CONTEXT_MENU,
		wxContextMenuEventHandler(my1Term::OnTermMenu),NULL,this);
	// thread for serial comm
	this->Bind(wxEVT_COMMAND_MYTHREAD_UPDATE,&my1Term::PortProcess,this);
	// popup menu event??
	this->Bind(wxEVT_COMMAND_MENU_SELECTED,&my1Term::OnCheckConf,this);
}
//------------------------------------------------------------------------------
my1Term::my1Term() : wxPanel(0x0,wxID_ANY) {
	// nothing to do?
}
//------------------------------------------------------------------------------
my1Term::~my1Term() {
	this->PortStop();
}
//------------------------------------------------------------------------------
#define WIN32LF 0x0A
#define WIN32CR 0x0D
//------------------------------------------------------------------------------
void my1Term::WriteSend(int aByte) {
	if (mOptions.mWin32CRLF&&aByte==WIN32LF) /* win32 newline '\n' */
		uart_send_byte(&mPortControl,WIN32CR); /* send win32 cr '\r' */
	uart_send_byte(&mPortControl,aByte); /* send */
	if (mOptions.mLocalEcho) {
		if (mOptions.mShowHex)
			this->WriteConsole(wxString::Format("[%02X]",aByte));
		else {
			if (aByte==WXK_BACK) {
				long cPos = mConsole->GetLastPosition();
				mConsole->Remove(cPos-1,cPos);
			}
			else this->WriteConsole((char) aByte);
		}
	}
}
//------------------------------------------------------------------------------
void my1Term::WriteNewLine(bool aNewLine) {
	if (aNewLine) this->WriteConsole(wxT("\n"));
	if (mDoCommand&&!uart_isopen(&mPortControl)) {
		this->WriteConsole(mPrompt);
		if(mCommand.Length())
			mCommand.Remove(0);
	}
}
//------------------------------------------------------------------------------
bool my1Term::ConfigPath(const wxString& aPath) {
	wxFileName cFileConf;
	cFileConf = wxFileName(aPath,mThisExec.GetName()+wxT("_term.conf"));
	if (!cFileConf.IsOk()) return false;
	mThisConf = cFileConf;
	return true;
}
//------------------------------------------------------------------------------
bool my1Term::ConfigFile(const wxString& aName) {
	wxFileName cFileConf;
	cFileConf = wxFileName(aName);
	if (!cFileConf.IsOk()) return false;
	mThisConf = cFileConf;
	return true;
}
//------------------------------------------------------------------------------
bool my1Term::LoadConfig(void) {
	bool cFlag = true;
	if (!mThisConf.FileExists()) return false;
	wxFileInputStream cRead(mThisConf.GetFullPath());
	wxFileConfig cSystem(cRead);
	wxString cConfigGroup;
	wxString cVal, cKey=wxT("/General/my1key");
	if (!cSystem.Read(cKey,&cVal)||cVal!=wxT("my1term"))
		return false;
	// load general options
	cConfigGroup = wxT("/General");
	if (cSystem.HasGroup(cConfigGroup)) {
		cSystem.SetPath(cConfigGroup);
		bool cTemp;
		wxString cKey;
		cKey = wxT("ShowHEX");
		if(cSystem.Read(cKey,&cTemp)) mOptions.mShowHex = cTemp;
		else cFlag = false;
		cKey = wxT("LocalEcho");
		if(cSystem.Read(cKey,&cTemp)) mOptions.mLocalEcho = cTemp;
		else cFlag = false;
		cKey = wxT("Win32CRLF");
		if(cSystem.Read(cKey,&cTemp)) mOptions.mWin32CRLF = cTemp;
		else cFlag = false;
		cSystem.SetPath(wxT("/"));
	}
	// load terminal options
	cConfigGroup = wxT("/Terminal");
	if (cSystem.HasGroup(cConfigGroup)) {
		cSystem.SetPath(cConfigGroup);
		wxString cKey,cVal;
		int cTempI;
		cKey = wxT("DeviceName");
		if(cSystem.Read(cKey,&cVal)) mOptions.mSerialConf.mDeviceName = cVal;
		else cFlag = false;
		cKey = wxT("PortNum");
		if(cSystem.Read(cKey,&cTempI)) mOptions.mSerialConf.mPortIndex = cTempI;
		else cFlag = false;
		cKey = wxT("BaudRate");
		if(cSystem.Read(cKey,&cTempI)) mOptions.mSerialConf.mBaudRate = cTempI;
		else cFlag = false;
		cSystem.SetPath(wxT("/")); // just in case
	}
	if (!cFlag)
		this->WriteConsole(wxT("\nCannot load all config!\n"));
	else {
		this->WriteConsole(wxT("\nConfig loaded!\n"));
		// copy to actual settings?
		strncpy(mPortControl.name,(const char*)
			mOptions.mSerialConf.mDeviceName.char_str(),MAX_COM_CHAR);
		mPortControl.term = mOptions.mSerialConf.mPortIndex;
		mPortConfig.baud = mOptions.mSerialConf.mBaudRate;
		// refresh port in case using new name!
		//this->PortFind();
		//this->UpdatePortMenu();
		this->UpdatePortList();
	}
	// check if saved port is not usable
	if (mOptions.mSerialConf.mPortIndex!=INVALID_PORT_INDEX) {
		int cTest = mOptions.mSerialConf.mPortIndex;
		if (!mOptions.mSerialConf.mPortExists[cTest-1]) {
			mOptions.mSerialConf.mPortIndex = INVALID_PORT_INDEX;
			mPortControl.term = INVALID_PORT_INDEX;
		}
	}
	return cFlag;
}
//------------------------------------------------------------------------------
bool my1Term::SaveConfig(void) {
	bool cFlag = true;
	wxString cConf = mThisConf.GetFullPath();
	if (!mThisConf.FileExists()) {
		wxFileOutputStream cTest(cConf);
		if (!cTest.IsOk()) {
			this->WriteConsole(wxT("\nCannot create config file!\n"));
			return false;
		}
	}
	wxFileInputStream *pFile = new wxFileInputStream(cConf);
	wxFileConfig cSystem(*pFile);
	// save general config
	cSystem.SetPath(wxT("/General"));
	{
		// throw in savefile id
		wxString cKey = wxT("my1key");
		wxString cVal = wxT("my1term");
		cFlag &= cSystem.Write(cKey,cVal);
		cKey = wxT("ShowHEX");
		cFlag &= cSystem.Write(cKey,mOptions.mShowHex);
		cKey = wxT("LocalEcho");
		cFlag &= cSystem.Write(cKey,mOptions.mLocalEcho);
		cKey = wxT("Win32CRLF");
		cFlag &= cSystem.Write(cKey,mOptions.mWin32CRLF);
		cSystem.SetPath(wxT("/"));
	}
	// save terminal config
	cSystem.SetPath(wxT("/Terminal"));
	{
		wxString cKey;
		cKey = wxT("DeviceName");
		cFlag &= cSystem.Write(cKey,mOptions.mSerialConf.mDeviceName);
		cKey = wxT("PortNum");
		cFlag &= cSystem.Write(cKey,mOptions.mSerialConf.mPortIndex);
		cKey = wxT("BaudRate");
		cFlag &= cSystem.Write(cKey,mOptions.mSerialConf.mBaudRate);
		cSystem.SetPath(wxT("/"));
	}
	delete pFile;
	pFile = 0x0; // just in case
	// only if no errors
	if (!cFlag)
		this->WriteConsole(wxT("\nCannot save config!\n"));
	else {
		this->WriteConsole(wxT("\nConfig saved!\n"));
		wxFileOutputStream cFile(cConf);
		cSystem.Save(cFile);
	}
	return cFlag;
}
//------------------------------------------------------------------------------
void my1Term::UpdatePortList(void) {
	if (!uart_isopen(&mPortControl)) {
		// only if no port is opened
		this->PortFind();
		// delete all menu items
		while (mMenuPort->GetMenuItemCount())
			mMenuPort->Destroy(mMenuPort->FindItemByPosition(0));
		mBoxT->Clear();
		// reinsert menu items
		mMenuPort->Append(MY1ID_TERM_PORTX,wxT("NONE"),
			wxEmptyString,wxITEM_RADIO);
		mBoxT->SetValue("Select Port");
		for (int cPort=1;cPort<=MAX_COM_PORT;cPort++) {
			if (mOptions.mSerialConf.mPortExists[cPort-1]) {
				wxString cBuff = mOptions.mSerialConf.mDeviceName +
					wxString::Format("%d",COM_PORT(cPort));
				mMenuPort->Append(MY1ID_TERM_PORTX+cPort,cBuff,
					wxEmptyString,wxITEM_RADIO);
				mBoxT->Append(cBuff.ToAscii(),(void*)
					(unsigned long)(MY1ID_TERM_PORTX+cPort));
			}
		}
		// check if no port available
		if (mBoxT->IsListEmpty()) {
			mBoxT->Enable(false);
			mTool->EnableTool(MY1ID_TERM_TOGL,false);
		}
		else {
			mBoxT->Enable(true);
			mTool->EnableTool(MY1ID_TERM_TOGL,true);
			int cPortID = mOptions.mSerialConf.mPortIndex;
			if (cPortID!=INVALID_PORT_INDEX) {
				wxString cBuff = mOptions.mSerialConf.mDeviceName +
					wxString::Format("%d",COM_PORT(cPortID));
				mBoxT->SetSelection(mBoxT->FindString(cBuff));
			}
		}
	}
}
//------------------------------------------------------------------------------
void my1Term::OnTermMenu(wxContextMenuEvent &event) {
	wxMenuItem* pItemPort = mMenuTerm->FindItem(MY1ID_TERM_PORT);
	wxMenuItem* pItemBaud = mMenuTerm->FindItem(MY1ID_TERM_BAUD);
	wxMenuItem* pItemLine = mMenuTerm->FindItem(MY1ID_TERM_LINE);
	wxMenuItem* pItemInit = mMenuTerm->FindItem(MY1ID_TERM_INIT);
	wxMenuItem* pItemStop = mMenuTerm->FindItem(MY1ID_TERM_STOP);
	wxMenuItem* pItemFind = mMenuTerm->FindItem(MY1ID_TERM_FIND);
	wxMenuItem* pItemSend = mMenuTerm->FindItem(MY1ID_TERM_SEND);
	wxMenuItem* pItemName = mMenuTerm->FindItem(MY1ID_TERM_DEVN);
	pItemName->SetItemLabel(mOptions.mSerialConf.mDeviceName);
	wxMenuItem* pItemLoad = mMenuTerm->FindItem(MY1ID_TERM_LOAD);
	wxMenuItem* pItemSave = mMenuTerm->FindItem(MY1ID_TERM_SAVE);
	if (uart_isopen(&mPortControl)) {
		pItemPort->Enable(false);
		pItemBaud->Enable(false);
		pItemLine->Enable(true);
		pItemInit->Enable(false);
		pItemStop->Enable(true);
		pItemFind->Enable(false);
		pItemSend->Enable(true);
		pItemName->Enable(false);
		pItemLoad->Enable(false);
		pItemSave->Enable(false);
	}
	else {
		pItemPort->Enable(true);
		pItemBaud->Enable(true);
		pItemLine->Enable(false);
		pItemInit->Enable(true);
		pItemStop->Enable(false);
		pItemFind->Enable(true);
		pItemSend->Enable(false);
		pItemName->Enable(true);
		pItemLoad->Enable(true);
		pItemSave->Enable(true);
	}
	// update current settings
	wxMenuItem* pItem;
	pItem = mMenuTerm->FindItem(MY1ID_TERM_SHOWHEX);
	pItem->Check(mOptions.mShowHex);
	pItem = mMenuTerm->FindItem(MY1ID_TERM_DOECHO);
	pItem->Check(mOptions.mLocalEcho);
	pItem = mMenuTerm->FindItem(MY1ID_TERM_WIN32LF);
	pItem->Check(mOptions.mWin32CRLF);
	int cPortID = mOptions.mSerialConf.mPortIndex;
	if (cPortID==INVALID_PORT_INDEX) cPortID = MY1ID_TERM_PORTX;
	else cPortID += MY1ID_TERM_PORTX;
	pItem = mMenuTerm->FindItem(cPortID);
	pItem->Check();
	int cBaudX_ID = MY1ID_TERM_BAUD0+mOptions.mSerialConf.mBaudRate;
	pItem = mMenuTerm->FindItem(cBaudX_ID);
	pItem->Check();
	// check if no port available
	if (mPortControl.term==INVALID_PORT_INDEX)
		pItemInit->Enable(false);
	this->PopupMenu(mMenuTerm);
}
//------------------------------------------------------------------------------
void my1Term::OnCheckConf(wxCommandEvent &event) {
	wxMenuItem *cItem;
	wxFileDialog *cSelect;
	wxTextEntryDialog* cDialog;
	int cObjectID = event.GetId();
	if (cObjectID==MY1ID_TERM_TOGL) {
		if (uart_isopen(&mPortControl))
			cObjectID = MY1ID_TERM_STOP;
		else cObjectID = MY1ID_TERM_INIT;
	}
	switch (cObjectID) {
		// port control
		case MY1ID_TERM_INIT:
			if (this->PortOpen()) {
				mBoxT->Enable(false);
				mConn->SetLabel("Disconnect");
				mConn->SetNormalBitmap(mIconStop);
				mTool->EnableTool(MY1ID_TERM_FIND,false);
				mTool->EnableTool(MY1ID_TERM_RATE,false);
				mTool->Refresh();
				mTool->Update();
				this->WriteConsole("\n\nConnected to ");
			}
			else this->WriteConsole("\n\nConnection failed to ");
			this->WriteConsole(this->TermSettings());
			this->WriteNewLine();
			break;
		case MY1ID_TERM_STOP:
			if (this->PortStop()) {
				mBoxT->Enable(true);
				mConn->SetLabel("Connect");
				mConn->SetNormalBitmap(mIconInit);
				mTool->EnableTool(MY1ID_TERM_FIND,true);
				mTool->EnableTool(MY1ID_TERM_RATE,true);
				this->WriteConsole("\nDisconnected from ");
			}
			else this->WriteConsole("\nDisconnection failed from ");
			this->WriteConsole(this->TermSettings(),true);
			this->WriteNewLine();
			break;
		case MY1ID_TERM_SEND:
			cSelect = new wxFileDialog(this,
				wxT("Select HEX file"),wxT(""),wxT(""),
				wxT("Any file (*.*)|*.*"),
				wxFD_OPEN|wxFD_FILE_MUST_EXIST|wxFD_CHANGE_DIR);
			cSelect->SetWildcard("HEX files (*.hex)|*.hex|Any file (*.*)|*.*");
			if (cSelect->ShowModal()==wxID_OK) {
				wxString cFileName = cSelect->GetPath();
				wxFile cFile(cFileName);
				bool cDone = true;
				if (cFile.IsOpened()) {
					this->WriteConsole(wxT("\nSending HEX file '"));
					this->WriteConsole(cFileName);
					this->WriteConsole(wxT("'...\n"));
					while (!cFile.Eof()) {
						int cData = 0x0;
						if (cFile.Read(&cData,1)!=1) {
							cDone = false;
							break;
						}
						this->WriteSend(cData);
					}
					cFile.Close();
					if (cDone) this->WriteConsole(wxT("Done Sending.\n"));
					else this->WriteConsole(wxT("Send Failed!\n"));
				}
				else {
					this->WriteConsole(wxT("\nCannot load/find HEX file '"));
					this->WriteConsole(cFileName);
					this->WriteConsole(wxT("'.\n"));
				}
			}
			break;
		case MY1ID_TERM_FIND:
			this->UpdatePortList();
			this->WriteConsole(wxT("\n\nPort(s) Found: ")+
				wxString::Format(wxT("%d\n"),mOptions.mSerialConf.mPortCount));
			this->WriteNewLine();
			break;
		case MY1ID_TERM_MENU:
			mTool->PopupMenu(mMenuTerm);
			break;
		case MY1ID_TERM_RATE:
			cItem = mMenuBaud->FindItem(MY1ID_TERM_RATE0+
				mOptions.mSerialConf.mBaudRate);
			cItem->Check();
			mTool->PopupMenu(mMenuBaud);
			break;
		case MY1ID_TERM_OPTS:
			cItem = mMenuOpts->FindItem(MY1ID_TERM_SHOWHEX1);
			cItem->Check(mOptions.mShowHex);
			cItem = mMenuOpts->FindItem(MY1ID_TERM_DOECHO1);
			cItem->Check(mOptions.mLocalEcho);
			cItem = mMenuOpts->FindItem(MY1ID_TERM_WIN32LF1);
			cItem->Check(mOptions.mWin32CRLF);
			mTool->PopupMenu(mMenuOpts);
			break;
		// general options
		case MY1ID_TERM_SHOWHEX:
			cItem = mMenuTerm->FindItem(cObjectID);
			mOptions.mShowHex = cItem->IsChecked();
			break;
		case MY1ID_TERM_DOECHO:
			cItem = mMenuTerm->FindItem(cObjectID);
			mOptions.mLocalEcho = cItem->IsChecked();
			break;
		case MY1ID_TERM_WIN32LF:
			cItem = mMenuTerm->FindItem(cObjectID);
			mOptions.mWin32CRLF = cItem->IsChecked();
			break;
		case MY1ID_TERM_SHOWHEX1:
			cItem = mMenuOpts->FindItem(cObjectID);
			mOptions.mShowHex = cItem->IsChecked();
			break;
		case MY1ID_TERM_DOECHO1:
			cItem = mMenuOpts->FindItem(cObjectID);
			mOptions.mLocalEcho = cItem->IsChecked();
			break;
		case MY1ID_TERM_WIN32LF1:
			cItem = mMenuOpts->FindItem(cObjectID);
			mOptions.mWin32CRLF = cItem->IsChecked();
			break;
		// editing options
		case MY1ID_TERM_COPY: mConsole->Copy(); break;
		case MY1ID_TERM_ZERO:
			mConsole->Clear();
			this->WriteNewLine(false);
			break;
		// load/save options
		case MY1ID_TERM_LOAD:
			if (!mThisConf.IsOk())
				this->WriteConsole(wxT("\nInvalid config file name!\n"));
			else {
				if (mThisConf.Exists()) {
					if (!this->LoadConfig())
						this->WriteConsole(wxS("\nLoadConfig failed! (")+
							mThisConf.GetFullPath()+wxS(")\n"));
				}
			}
			break;
		case MY1ID_TERM_SAVE:
			if (!mThisConf.IsOk())
				this->WriteConsole(wxT("\nInvalid config file name!\n"));
			else {
				if (!this->SaveConfig())
					this->WriteConsole(wxS("\nSaveConfig failed! (")+
						mThisConf.GetFullPath()+wxS(")\n"));
			}
			break;
		// terminal options
		case MY1ID_TERM_BAUD0:
		case MY1ID_TERM_BAUD1:
		case MY1ID_TERM_BAUD2:
		case MY1ID_TERM_BAUD3:
		case MY1ID_TERM_BAUD4:
		case MY1ID_TERM_BAUD5:
		case MY1ID_TERM_BAUD6:
		case MY1ID_TERM_BAUD7:
			mOptions.mSerialConf.mBaudRate = cObjectID - MY1ID_TERM_BAUD0;
			mPortConfig.baud = mOptions.mSerialConf.mBaudRate;
			break;
		case MY1ID_TERM_DEVN:
			cDialog = new wxTextEntryDialog(this,
				wxT("Enter New Device Name"), wxT("Serial Device Name"));
			if (cDialog->ShowModal()==wxID_OK) {
				mOptions.mSerialConf.mDeviceName = cDialog->GetValue();
				strncpy(mPortControl.name,
					(const char*)mOptions.mSerialConf.mDeviceName.char_str(),
					MAX_COM_CHAR);
			}
			break;
		// alt rate
		case MY1ID_TERM_RATE0:
		case MY1ID_TERM_RATE1:
		case MY1ID_TERM_RATE2:
		case MY1ID_TERM_RATE3:
		case MY1ID_TERM_RATE4:
		case MY1ID_TERM_RATE5:
		case MY1ID_TERM_RATE6:
		case MY1ID_TERM_RATE7:
			mOptions.mSerialConf.mBaudRate = cObjectID - MY1ID_TERM_RATE0;
			mPortConfig.baud = mOptions.mSerialConf.mBaudRate;
			break;
		default:
			// port selection id is dynamic...
			if (cObjectID>=MY1ID_TERM_PORT0&&
					cObjectID<MY1ID_TERM_PORT0+MAX_COM_PORT) {
				mOptions.mSerialConf.mPortIndex = cObjectID-MY1ID_TERM_PORT0+1;
				mPortControl.term = mOptions.mSerialConf.mPortIndex;
			}
			break;
	}
}
//------------------------------------------------------------------------------
void my1Term::OnKeyConsole(wxKeyEvent& event) {
	int cKeyCode, cTest;
	bool cValid;
	cValid = false;
	cKeyCode = event.GetKeyCode();
	switch (cKeyCode) {
		case WXK_BACK:
			cTest = mConsole->GetNumberOfLines();
			cTest = mConsole->GetLineLength(cTest-1); // last line!
			if (!cTest) break;
		case WXK_RETURN: case WXK_TAB: case WXK_SPACE:
			cValid = true;
			break;
		default:
			if ((cKeyCode>=0x41&&cKeyCode<=0x5A)||
					(cKeyCode>=0x61&&cKeyCode<=0x7A)||
					(cKeyCode>=0x30&&cKeyCode<=0x39)||
					(cKeyCode>=0x20&&cKeyCode<=0x2F)||
					(cKeyCode=='?')) {
				cValid =true;
			}
	}
	if (cValid) {
		if (uart_isopen(&mPortControl)) this->WriteSend(cKeyCode);
		else if (mDoCommand) {
			// command mode???
			if (cKeyCode==WXK_BACK) {
				if (mCommand.Length()) {
					mCommand.RemoveLast();
					long cPos = mConsole->GetLastPosition();
					mConsole->Remove(cPos-1,cPos);
				}
			}
			else if(cKeyCode==WXK_RETURN) {
				// process command??
				mCommand.Trim(); // right
				mCommand.Trim(false); // left
				if (OnCommand) (*OnCommand)(this);
				mCommand.Remove(0);
				this->WriteConsole("\n");
				this->WriteConsole(mPrompt);
			}
			else {
				mCommand.Append((char)cKeyCode);
				this->WriteConsole((char)cKeyCode);
			}
		}
	}
}
//------------------------------------------------------------------------------
void my1Term::WriteConsole(const wxString& aString,bool aNewLine) {
	mConsole->AppendText(aString);
	if (aNewLine) mConsole->AppendText("\n");
}
//------------------------------------------------------------------------------
void my1Term::SetPrompt(const wxString& aString) {
	mPrompt = aString;
	if (mPrompt.Length()) {
		if (!mDoCommand&&!uart_isopen(&mPortControl)) {
			this->WriteConsole("\n");
			this->WriteConsole(mPrompt);
		}
		mDoCommand = true;
	}
	else mDoCommand = false;
}
//------------------------------------------------------------------------------
wxString& my1Term::TermSettings(void) {
	mTermSettings.Remove(0);
	if (mPortControl.term>0&&mPortControl.term<=MAX_COM_CHAR) {
		mTermSettings << wxString::Format("%s%d",mPortControl.name,
			COM_PORT(mPortControl.term));
		int cBaudRate = uart_actual_baudrate(mPortConfig.baud);
		mTermSettings << wxString::Format(" (%d-8N1)",cBaudRate);
	}
	else mTermSettings << "NONE";
	return mTermSettings;
}
//------------------------------------------------------------------------------
wxString& my1Term::GetCommand(void) {
	return mCommand;
}
//------------------------------------------------------------------------------
void my1Term::Editable(bool edit) {
	mConsole->SetEditable(edit);
}
//------------------------------------------------------------------------------
wxToolBar* my1Term::Toolbar(void) {
	return this->mTool;
}
//------------------------------------------------------------------------------
bool my1Term::PortFind(int anIndex) {
	if (anIndex>=1&&anIndex<=MAX_COM_PORT)
		return uart_prep(&mPortControl,anIndex);
	mOptions.mSerialConf.mPortCount = 0;
	anIndex = 0;
	for (int cTest=1;cTest<=MAX_COM_PORT;cTest++) {
		if (uart_prep(&mPortControl,cTest)) {
			mOptions.mSerialConf.mPortExists[cTest-1] = true;
			mOptions.mSerialConf.mPortCount++;
			if (!anIndex) anIndex = cTest;
		}
		else mOptions.mSerialConf.mPortExists[cTest-1] = false;
	}
	if (anIndex&&mPortControl.term!=anIndex)
		uart_prep(&mPortControl,anIndex);
	return anIndex?true:false;
}
//------------------------------------------------------------------------------
bool my1Term::PortInit(void) {
	uart_init(&mPortControl);
	uart_get_config(&mPortControl,&mPortConfig);
	return this->PortFind();
}
//------------------------------------------------------------------------------
bool my1Term::PortOpen(void) {
	bool cStatus = false;
	my1uart_conf_t cPortConfig;
	uart_get_config(&mPortControl,&cPortConfig);
	uart_set_config(&mPortControl,&mPortConfig);
	if (uart_open(&mPortControl)) {
		mThreadChk = new my1wxThreadCOM(this);
		if (mThreadChk->Create()!=wxTHREAD_NO_ERROR) {
			wxLogError("Cannot create the thread!");
			delete mThreadChk;
			mThreadChk = 0x0;
		}
		else {
			if (mThreadChk->Run()!=wxTHREAD_NO_ERROR) {
				wxLogError("Cannot run the thread!");
				delete mThreadChk;
				mThreadChk = 0x0;
			}
		}
		if (mThreadChk) cStatus = true;
		else uart_done(&mPortControl);
	}
	if (!cStatus) uart_set_config(&mPortControl,&cPortConfig);
	return cStatus;
}
//------------------------------------------------------------------------------
bool my1Term::PortStop(void) {
	bool cStatus = true;
	if (uart_isopen(&mPortControl)) {
		cStatus = false;
		if (mThreadChk) {
			mThreadChk->Delete();
			while (mThreadRunning);
			//delete mThreadChk; // not needed??
			mThreadChk = 0x0;
		}
		if (uart_done(&mPortControl))
			cStatus = true;
	}
	return cStatus;
}
//------------------------------------------------------------------------------
bool my1Term::PortData(void) {
	bool cStatus = false;
	if (!mCheckingData) {
		int cCount = uart_incoming(&mPortControl);
		if (cCount>0) cStatus = true;
	}
	return cStatus;
}
//------------------------------------------------------------------------------
void my1Term::PortProcess(wxThreadEvent& event) {
	mCheckingData = true;
	while (uart_incoming(&mPortControl)) {
		byte08_t cTemp = uart_read_byte(&mPortControl);
		if (mOptions.mWin32CRLF&&cTemp==WIN32CR) // skip this
			continue;
		if (mOptions.mShowHex)
			this->WriteConsole(wxString::Format("[%02X]",cTemp));
		else this->WriteConsole((char)cTemp);
	}
	mCheckingData = false;
}
//------------------------------------------------------------------------------
void my1Term::ThreadRunning(bool aRun) {
	mThreadRunning = aRun;
}
//------------------------------------------------------------------------------
my1wxThreadCOM::my1wxThreadCOM(my1Term *aTerm)
		: wxThread(wxTHREAD_DETACHED), mTerm(aTerm) {
	// nothing to do?
}
//------------------------------------------------------------------------------
my1wxThreadCOM::~my1wxThreadCOM() {
	// nothing to do?
}
//------------------------------------------------------------------------------
wxThread::ExitCode my1wxThreadCOM::Entry() {
	mTerm->ThreadRunning();
	while (!TestDestroy()) {
		if (mTerm->PortData()) {
			wxThreadEvent *pEvent =
				new wxThreadEvent(wxEVT_COMMAND_MYTHREAD_UPDATE);
			wxQueueEvent(mTerm,pEvent);
		}
	}
	mTerm->ThreadRunning(false);
	return (wxThread::ExitCode) 0; // success
}
//------------------------------------------------------------------------------
