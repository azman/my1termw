//------------------------------------------------------------------------------
/**
* wxterm.hpp
* - header for wx-based terminal interface panel
**/
//------------------------------------------------------------------------------
#ifndef __MY1TERM_HPP__
#define __MY1TERM_HPP__
//------------------------------------------------------------------------------
#include <wx/wx.h>
#include <wx/thread.h>
#include <wx/filename.h>
//------------------------------------------------------------------------------
extern "C" {
#include "my1uart.h"
}
//------------------------------------------------------------------------------
#define MY1ID_TERM_OFFSET (wxID_HIGHEST+801)
//------------------------------------------------------------------------------
enum {
	MY1ID_TERM = MY1ID_TERM_OFFSET,
	MY1ID_TERM_INIT,
	MY1ID_TERM_STOP,
	MY1ID_TERM_TOGL,
	MY1ID_TERM_FIND,
	MY1ID_TERM_MENU,
	MY1ID_TERM_LOAD,
	MY1ID_TERM_SAVE,
	MY1ID_TERM_SEND,
	MY1ID_TERM_PORT,
	MY1ID_TERM_BAUD,
	MY1ID_TERM_RATE,
	MY1ID_TERM_OPTS,
	MY1ID_TERM_LINE,
	MY1ID_TERM_NAME,
	MY1ID_TERM_DTR1,
	MY1ID_TERM_DSR1,
	MY1ID_TERM_RTS1,
	MY1ID_TERM_CTS1,
	MY1ID_TERM_COPY,
	MY1ID_TERM_PASTE,
	MY1ID_TERM_ZERO,
	MY1ID_TERM_SHOWHEX,
	MY1ID_TERM_DOECHO,
	MY1ID_TERM_WIN32LF,
	MY1ID_TERM_SHOWHEX1,
	MY1ID_TERM_DOECHO1,
	MY1ID_TERM_WIN32LF1,
	MY1ID_TERM_PORTX,
	MY1ID_TERM_PORT0,
	MY1ID_TERM_BAUD0 = MY1ID_TERM_PORT0 + MAX_COM_PORT,
	MY1ID_TERM_BAUD1,
	MY1ID_TERM_BAUD2,
	MY1ID_TERM_BAUD3,
	MY1ID_TERM_BAUD4,
	MY1ID_TERM_BAUD5,
	MY1ID_TERM_BAUD6,
	MY1ID_TERM_BAUD7,
	MY1ID_TERM_RATE0,
	MY1ID_TERM_RATE1,
	MY1ID_TERM_RATE2,
	MY1ID_TERM_RATE3,
	MY1ID_TERM_RATE4,
	MY1ID_TERM_RATE5,
	MY1ID_TERM_RATE6,
	MY1ID_TERM_RATE7,
	MY1ID_TERM_DEVN,
	MY1ID_TERM_DUMMY
};
//------------------------------------------------------------------------------
struct uartcfg_t {
	int mBaudRate, mPortIndex, mPortCount;
	bool mPortExists[MAX_COM_PORT],mChanged;
	wxString mDeviceName;
	bool operator==(uartcfg_t& rConfig) {
		mChanged = true;
		if ((mBaudRate==rConfig.mBaudRate)&&
				(mPortIndex==rConfig.mPortIndex)&&
				(mDeviceName==rConfig.mDeviceName))
			mChanged = false;
		return !mChanged;
	}
};
//------------------------------------------------------------------------------
struct uartopt_t {
	bool mChanged,mShowHex,mLocalEcho,mWin32CRLF;
	uartcfg_t mSerialConf;
	bool operator!=(uartopt_t& aTermOpts) {
		mChanged = true;
		if ((mSerialConf==aTermOpts.mSerialConf)&&
				(mLocalEcho==aTermOpts.mLocalEcho)&&
				(mWin32CRLF==aTermOpts.mWin32CRLF)&&
				(mShowHex==aTermOpts.mShowHex))
			mChanged = false;
		return mChanged;
	}
};
//------------------------------------------------------------------------------
class my1wxThreadCOM;
//------------------------------------------------------------------------------
class my1Term : public wxPanel {
	wxDECLARE_DYNAMIC_CLASS(my1Term);
	// maybe i can draw my own terminal interface on canvas!?
protected:
	my1uart_t mPortControl;
	my1uart_conf_t mPortConfig;
	my1wxThreadCOM *mThreadChk;
	bool mThreadRunning, mCheckingData, mDoCommand;
	uartopt_t mOptions;
	wxTextCtrl *mConsole;
	wxString mCommand, mPrompt, mTermSettings;
	wxFileName mThisExec,mThisConf;
	wxMenu *mMenuTerm, *mMenuPort, *mMenuBaud, *mMenuOpts;
	wxToolBarToolBase *mConn;
	wxToolBar* mTool;
	wxComboBox *mBoxT;
	wxBitmap mIconInit, mIconStop;
public:
	my1Term(wxWindow*,wxWindowID,const wxPoint& point = wxDefaultPosition,
		const wxSize& size = wxDefaultSize);
	my1Term(); // needed by wxIMPLEMENT_DYNAMIC_CLASS
	~my1Term();
	void WriteSend(int aByte);
	void WriteNewLine(bool aNewLine=true);
	bool ConfigPath(const wxString& aPath);
	bool ConfigFile(const wxString& aName);
	bool LoadConfig(void);
	bool SaveConfig(void);
	void UpdatePortList(void);
	void OnTermMenu(wxContextMenuEvent &event);
	void OnCheckConf(wxCommandEvent &event);
	void OnKeyConsole(wxKeyEvent&);
	void WriteConsole(const wxString&,bool aNewLine=false);
	void (*OnCommand)(void*);
	void SetPrompt(const wxString&);
	wxString& TermSettings(void);
	wxString& GetCommand(void);
	void Editable(bool);
	wxToolBar* Toolbar(void);
public:
	// terminal@port methods
	bool PortFind(int anIndex=INVALID_PORT_INDEX);
	bool PortInit(void);
	bool PortOpen(void);
	bool PortStop(void);
	bool PortData(void);
	void PortProcess(wxThreadEvent&);
	// should only be called by thread!
	void ThreadRunning(bool aRun=true);
};
//------------------------------------------------------------------------------
class my1wxThreadCOM : public wxThread {
protected:
	my1Term *mTerm;
public:
	my1wxThreadCOM(my1Term*);
	~my1wxThreadCOM();
protected:
	virtual ExitCode Entry();
};
//------------------------------------------------------------------------------
#endif /** __MY1TERM_HPP__ */
//------------------------------------------------------------------------------
