//------------------------------------------------------------------------------
/**
* wxform.hpp
* - header for main wx-based form
**/
//------------------------------------------------------------------------------
#ifndef __MY1FORM_HPP__
#define __MY1FORM_HPP__
//------------------------------------------------------------------------------
#include "wxmain.hpp"
#include "wxterm.hpp"
//------------------------------------------------------------------------------
#define MY1APP_TITLE "MY1 Serial Interface"
#define MY1APP_PROGNAME "my1termw"
#ifndef APP_PROGVERS
#define MY1APP_PROGVERS "build"
#else
#define MY1APP_PROGVERS APP_PROGVERS
#endif
#ifndef APP_LICENSE_
#define MY1APP_LICENSE_ "Azman M. Yusof"
#else
#define MY1APP_LICENSE_ APP_LICENSE_
#endif
//------------------------------------------------------------------------------
#define MY1ID_MAIN_OFFSET (wxID_HIGHEST+1)
//------------------------------------------------------------------------------
enum {
	MY1ID_MAIN = MY1ID_MAIN_OFFSET,
	MY1ID_EXIT,
	MY1ID_ABOUT,
	MY1ID_MAIN_TOOL,
	MY1ID_DUMMY
};
//------------------------------------------------------------------------------
class my1Form : public wxFrame {
private:
	my1Term* mTerm;
public:
	my1Form(const wxString& title);
	~my1Form();
	void OnExit(wxCommandEvent&);
	void OnAbout(wxCommandEvent&);
};
//------------------------------------------------------------------------------
#endif /** __MY1FORM_HPP__ */
//------------------------------------------------------------------------------
