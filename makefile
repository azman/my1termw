# makefile for my1termw - gui for serial port terminal

PROJECT = my1termw
GUISBIN = $(PROJECT)
GUISPRO = $(GUISBIN)
GUISOBJ = my1uart.o wxterm.o wxform.o wxmain.o

EXTPATH = ../my1codelib/src
PACKDIR = $(PROJECT)-$(shell cat VERSION)
PACKDAT = README VERSION
PLATBIN ?= $(shell uname -m)
VERSION = -DAPP_PROGVERS="\"$(shell date +%Y%m%d)\""
LICENSE = -DAPP_LICENSE_="\"$(shell sed -n "s/^.*(c) \(.*\)$$/\1/p" LICENSE)\""

DELETE = rm -rf
COPY = cp -R
ARCHIVE = tar cjf
ARCHEXT = .tar.bz2
CONVERT = convert

CFLAGS += -Wall -I$(EXTPATH)
LFLAGS +=
OFLAGS +=
LOCAL_FLAGS =
#WX_LIBS = stc,html,adv,core,xml,base
WX_LIBS = base,core,adv
#WX_STAT = --static=yes
WX_LIBFLAGS = $(shell wx-config --libs $(WX_LIBS) $(WX_STAT))
WX_CXXFLAGS = $(shell wx-config --cxxflags)

ifeq ($(DO_MINGW),YES)
	GUISPRO = $(GUISBIN).exe
	GUISOBJ += wxmain.res
	PLATBIN = mingw
	ARCHIVE = zip -r
	ARCHEXT = .zip
	# cross-compiler settings
	XTOOL_DIR ?= /home/share/tool/mingw
	XTOOL_TARGET = $(XTOOL_DIR)
	CROSS_COMPILE = $(XTOOL_TARGET)/bin/i686-pc-mingw32-
	# extra switches
	CFLAGS += -I$(XTOOL_DIR)/include -static -DDO_MINGW -DWIN32_LEAN_AND_MEAN
	LFLAGS += -L$(XTOOL_DIR)/lib
	# below is to remove console at runtime - do we really need this??
	LFLAGS += -Wl,-subsystem,windows
	# can't remember why, but '-mthreads' is not playing nice with others - has to go!
	WX_LIBFLAGS = $(shell $(XTOOL_DIR)/bin/wx-config --libs $(WX_LIBS) | sed 's/-mthreads//g')
	WX_CXXFLAGS = $(shell $(XTOOL_DIR)/bin/wx-config --cxxflags | sed 's/-mthreads//g')
	# include for resource compilation!
	WINDRES_FLAG = --include-dir $(XTOOL_DIR)/include --include-dir $(XTOOL_DIR)/include/wx-2.9
endif

PACKDAT += $(GUISPRO) $(CONSPRO)
CC = $(CROSS_COMPILE)gcc
CPP = $(CROSS_COMPILE)g++
RES = $(CROSS_COMPILE)windres
debug: LOCAL_FLAGS += -DMY1DEBUG
pack-src: ARCNAME = $(PACKDIR)-src-$(shell date +%Y%m%d)$(ARCHEXT)
pack: ARCNAME = $(PACKDIR)-$(PLATBIN)-$(shell date +%Y%m%d)$(ARCHEXT)
version: VERSION = -DMY1APP_PROGVERS="\"$(shell cat VERSION)\""

main: $(GUISPRO)

all: main

run: main
	$(GUISPRO)

new: clean all

debug: new

version: new

pack-src:
	git archive --format=tar --prefix=$(PROJECT)/ HEAD | bzip2 >$(ARCNAME)

pack: version
	mkdir -pv $(PACKDIR)
	$(COPY) $(PACKDAT) $(PACKDIR)/
	$(DELETE) $(ARCNAME)
	$(ARCHIVE) $(ARCNAME) $(PACKDIR)

$(GUISPRO): $(GUISOBJ)
	$(CPP) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS) $(WX_LIBFLAGS)

wx%.o: src/wx%.cpp src/wx%.hpp
	$(CPP) $(CFLAGS) $(VERSION) $(LICENSE) $(WX_CXXFLAGS) -c $<

wx%.o: src/wx%.cpp
	$(CPP) $(CFLAGS) $(VERSION) $(LICENSE) $(WX_CXXFLAGS) -c $<

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.c
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.cpp src/%.hpp
	$(CPP) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.cpp
	$(CPP) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.ico: res/%.xpm
	$(CONVERT) $< $@

%.res: src/%.rc apps.ico
	$(RES) --include-dir res $(WINDRES_FLAG) -O COFF $< -o $@

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EXTPATH)/%.c
	$(CC) $(CFLAGS) -c $<

clean:
	-$(DELETE) $(GUISPRO) $(PACKDIR) *.exe *.bz2 *.zip *.o *.ico *.res
